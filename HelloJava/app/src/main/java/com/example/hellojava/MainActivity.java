package com.example.hellojava;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @SuppressLint({"ResourceAsColor", "SetTextI18n"})
    public void setHelloJavaText(View view) {
        TextView helloTextView = findViewById(R.id.nameTextView);
        helloTextView.setText("Hello Java!");
        helloTextView.setBackgroundColor(Color.YELLOW);
        helloTextView.setTextSize(30);
    }
}
