package com.example.musicshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class OrderActivity extends AppCompatActivity {

    String[] addresses = {"ollywool@gmail.com"};
    String subject = "Order from Ollywool mobile Shop";
    String emailText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        Intent recievedToOrderIntent = getIntent();
        String username = recievedToOrderIntent.getStringExtra("userNameForIntent");
        String goodsName = recievedToOrderIntent.getStringExtra("goodsName");
        int quantity = recievedToOrderIntent.getIntExtra("quantity", 0);
        double orderPrice = recievedToOrderIntent.getDoubleExtra("orderPrice",0);
        emailText = "Customer name: "+ username +
                "\n" +"Goods name: "+  goodsName +
                "\n" + "Quantity: " + quantity +
                "\n" + "Price: " +  orderPrice +
                "\n" + "Oreder price: " + quantity*orderPrice;
        TextView orderTextView = findViewById(R.id.orderTextView);
        orderTextView.setText(emailText);


    }

    public void submitOrder(View view) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, emailText);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


}
