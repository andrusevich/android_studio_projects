package com.example.musicshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    int quantity = 0;
    Spinner spinner;
    ArrayList spinnerArrayList;
    ArrayAdapter spinnerAdaptor;

    HashMap goodsMap;
    String goodsName;
    double price;
    EditText userNameEditText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createSpinner();
        createMAp();

        userNameEditText = findViewById(R.id.userName);

        spinnerAdaptor = new ArrayAdapter(this, android.R.layout.simple_spinner_item,spinnerArrayList);
        spinnerAdaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdaptor);


    }

    void createMAp()
    {
        goodsMap = new HashMap();
        goodsMap.put("Toys", 100.0);
        goodsMap.put("Rugs", 150.0);
        goodsMap.put("Caps", 70.0);
        goodsMap.put("Clothes", 65.0);
        goodsMap.put("Other", 100.0);

    }

    void createSpinner()
    {
        spinner = findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
        spinnerArrayList = new ArrayList();
       /*
        spinnerArrayList.add("Игрушки");
        spinnerArrayList.add("Коврики");
        spinnerArrayList.add("Шапочки");
        spinnerArrayList.add("Обмоточки");
        spinnerArrayList.add("Прочее");
        */
        spinnerArrayList.add("Toys");
        spinnerArrayList.add("Rugs");
        spinnerArrayList.add("Caps");
        spinnerArrayList.add("Clothes");
        spinnerArrayList.add("Other");
    }

    public void increaseQuantity(View view) {
        TextView quantityTextView = findViewById(R.id.quantityTextView);
        quantity = quantity+1;
        quantityTextView.setText("" + quantity);
        TextView priceTextView = findViewById(R.id.priceTextView);
        priceTextView.setText(""+quantity*price);
    }

    public void decreaseQuantity(View view) {
        TextView quantityTextView = findViewById(R.id.quantityTextView);
        quantity = quantity-1;
        TextView priceTextView = findViewById(R.id.priceTextView);
        priceTextView.setText(""+quantity*price);
        // нкжно решить проблему с - значением цены
        if (quantity<0)
        {
            quantity=0;
        }
        quantityTextView.setText("" + quantity);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        goodsName = spinner.getSelectedItem().toString();
        price = (double)goodsMap.get(goodsName);
        TextView priceTextView = findViewById(R.id.priceTextView);
        priceTextView.setText(""+quantity*price);
        ImageView goodsImageView = findViewById(R.id.goodsImageView);

        switch (goodsName)
        {
            case "Toys":
                goodsImageView.setImageResource(R.drawable.barashka);
            break;
            case "Rugs":
                goodsImageView.setImageResource(R.drawable.rug);
                break;
            case "Caps":
                goodsImageView.setImageResource(R.drawable.cap);
                break;
            case "Clothes":
                goodsImageView.setImageResource(R.drawable.cloth);
                break;
            case "Other":
                goodsImageView.setImageResource(R.drawable.other);
                break;
            default:
                goodsImageView.setImageResource(R.drawable.barashka);
                break;
        }



    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void addToCard(View view) {
        Order order = new Order();
        order.userName = userNameEditText.getText().toString();
        Log.d("userName",  order.userName);
        order.goodsName = goodsName;
        Log.d("goodsName",  order.goodsName);
        order.quantity = quantity;
        Log.d("quantity",  ""+ order.quantity);
        order.orderPrice = quantity*price;
        Log.d("orderPrice",  ""+ order.orderPrice);

        Intent orderIntent = new Intent(MainActivity.this, OrderActivity.class);
        orderIntent.putExtra("userNameForIntent", order.userName);
        orderIntent.putExtra("goodsName", order.goodsName);
        orderIntent.putExtra("quantity", order.quantity);
        orderIntent.putExtra("orderPrice", order.orderPrice);

        startActivity(orderIntent);


    }
}
